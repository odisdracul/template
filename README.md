# Template

 The first tool I made to solve a problem.

## Manual

### This script creates templates.
### Usage:
> template [arg]

### Arguments:
> perl - creates perl template
>
> python - creates python template
>
> go - creates golang tempalte
>
> sh - creates a bash template
>
> notes - creates a note template
>
> docker - creates black dockerfile
>
> rust - creates rust template
>
> docker-compose - creates docker-compose template
